from __future__ import absolute_import, unicode_literals
import os

from celery import Celery
from django.conf import settings


os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'restaurant.settings')


app = Celery('restaurant')
app.config_from_object(settings, namespace='CELERY')

app.conf.beat_schedule = {
    'print-checks-every-10-seconds': {
        'task': 'api.tasks.print_rendered_checks',
        'schedule': 10.0
    }
}

app.autodiscover_tasks()
