from django.contrib import admin

from .models import Printer, Check

# Register your models here.

admin.register(Printer)


@admin.register(Check)
class CheckAdmin(admin.ModelAdmin):
    list_filter = ('printer_id', 'type', 'status')