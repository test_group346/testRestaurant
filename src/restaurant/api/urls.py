from django.urls import path
from .views import get_point_ids, new_order


urlpatterns = [
    path('points_ids/', get_point_ids),
    path('new_order/', new_order)
]
