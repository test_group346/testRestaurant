import base64
import json
from pathlib import Path

from celery import shared_task
from django.conf import settings
import requests
from django.template.loader import render_to_string

from .models import Check, STATUS_CHOICES


@shared_task(bind=True)
def create_pdf_file(self, check_id: int) -> None:
    check = Check.objects.get(id=check_id)
    data_string = render_to_string(
        template_name='check.html',
        context={
            'type': check.type,
            'point_id': check.printer_id.point_id,
            'order': check.order
        }
    )
    encoded_string = base64.b64encode(bytes(data_string, encoding='utf-8'))
    headers = {'Content-Type': 'application/json'}
    data = {'contents': encoded_string.decode('utf-8')}
    response = requests.post(
        url=settings.WKHTMLTOPDF_URL,
        data=json.dumps(data),
        headers=headers
    )
    filename = f"{check.id}_{check.type}.pdf"
    filepath = settings.MEDIA_ROOT / Path('pdf') / Path(filename)

    with open(filepath, 'wb') as pdf_file:
        pdf_file.write(response.content)

    check.pdf_file = str(filepath)
    check.status = STATUS_CHOICES.RENDERED
    check.save()
    return 'Done'


@shared_task(bind=True)
def print_rendered_checks(self) -> None:
    """
    printing all recent rendered checks
    :param self:
    :return:
    """
    rendered_checks = Check.objects.\
        filter(status=STATUS_CHOICES.RENDERED).all()

    for check in rendered_checks:
        check.status = STATUS_CHOICES.PRINTED
        check.save()
