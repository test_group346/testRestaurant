from django.http import JsonResponse
from rest_framework.decorators import api_view
from rest_framework.request import Request

from .tasks import create_pdf_file
from .models import Printer, Check

# Create your views here.


@api_view(['POST'])
def new_order(request: Request) -> JsonResponse:

    params = dict(request.query_params)
    try:
        point_id = params['point_id'][0]
    except KeyError:
        return JsonResponse({'detail': "No point id"})

    printers = Printer.objects.filter(point_id=point_id).all()

    if not printers:
        return JsonResponse({'detail': "Point have not any printer"})

    order_data = request.data['order']

    saved_checks = Check.objects.filter(
        order=order_data,
        printer_id__point_id=point_id
    ).all()

    if saved_checks:
        return JsonResponse({'detail': "This order already have checks"})

    for printer in printers:
        check = Check(
            printer_id=printer,
            type=printer.check_type,
            order=order_data
        )
        check.save()
        create_pdf_file.delay(check_id=check.id)

    return JsonResponse({'detail': "Successfully generating checks"})


@api_view(['GET'])
def get_point_ids(_) -> JsonResponse:
    printers = Printer.objects.all()
    point_ids = set()

    for printer in printers:
        point_ids.add(printer.point_id)

    return JsonResponse({'point_ids': point_ids})
