from django.db import models

# Create your models here.


class TYPE_CHOICES(models.TextChoices):

    KITCHEN = 'kitchen', "Kitchen"
    CLIENT = 'client', "Client"


class STATUS_CHOICES(models.TextChoices):

    NEW = 'new', "Now created check"
    RENDERED = 'rendered', "Rendered to pdf"
    PRINTED = 'printed', 'Printed on printer'


class Printer(models.Model):

    name = models.CharField(null=False, max_length=50)
    api_key = models.CharField(null=False, max_length=50)
    check_type = models.CharField(choices=TYPE_CHOICES.choices, null=False, max_length=10)
    point_id = models.IntegerField(null=False)

    def __str__(self):
        return f"{self.name} - {self.point_id}"


class Check(models.Model):

    printer_id = models.ForeignKey(Printer, on_delete=models.CASCADE)
    type = models.CharField(choices=TYPE_CHOICES.choices, null=False, max_length=10)
    order = models.JSONField(null=False)
    status = models.CharField(
        choices=STATUS_CHOICES.choices,
        null=False,
        default=STATUS_CHOICES.NEW,
        max_length=10
    )
    pdf_file = models.CharField(max_length=100)
