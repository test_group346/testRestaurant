cd src
cd restaurant
python manage.py makemigrations
python manage.py migrate
python manage.py loaddata fixtures/initial.json --app api.Printer
python manage.py runserver 8000