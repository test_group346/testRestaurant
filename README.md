## Install and run

#### First console
```bash
    install.sh
    run_infrastructure.sh
```

#### Second console
```bash
    run_django.sh
```

#### Third console
```bash
    run_celery_worker.sh
```

#### Fourth console
```bash
    run_celery_beat.sh
```

And for simple test API you can use `test.py` file
